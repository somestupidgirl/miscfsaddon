#ifndef _SYS_SYSCTL_BSD_H_
#define	_SYS_SYSCTL_BSD_H_

#define	CTLFLAG_MPSAFE	0x00040000	/* Handler is MP safe */

#endif /* _SYS_SYSCTL_BSD_H_ */